(() => {
  "use strict";

  const THREE = require("three");
  const controls = require("three/examples/jsm/controls/OrbitControls");
  THREE.OrbitControls = controls.OrbitControls;
  THREE.MapControls = controls.MapControls;

  module.exports = THREE;
})();
